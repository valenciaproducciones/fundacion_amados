@php
    $logged_user = Auth::user();
@endphp

<div id="app-nav-foot" class="hidden-menu">
    <div class="footer_container">

        <div class="logo_footer"></div>

			<div class="footer">
				<div class="social mb-2">
					<a href="https://www.facebook.com/smartfilmsco" rel="" title="Síguenos en Facebook"><i class="fab fa-facebook-f"></i></a>
					<a href="https://twitter.com/smartfilmsco" rel="" title="Síguenos en Twitter"><i class="fab fa-twitter"></i></a>
					<a href="https://www.youtube.com/user/smartphonefilmf" rel="" title="Síguenos en Youtube"><i class="fab fa-youtube"></i></a>
					<a href="https://www.instagram.com/smartfilmsco/" rel="" title="Síguenos en Instagram"><i class="fab fa-instagram"></i></a>
				</div>
				<div class="info">
					<span class="title mb-2">CONTÁCTANOS</span>
					<p class="text mb-2">
						Dirección: Cra 22 No. 142 - 58<br>
						Teléfono: (57 1) 467 24 37 / 641 77 71<br>
						E-mail: info@smartfilms.com.co<br>
						<b>Bogotá - Colombia</b>
					</p>
					<button type="button" class="btn btn-outline-dark">Política de privacidad</button>
				</div>
			</div>

		   
    </div>
</div>