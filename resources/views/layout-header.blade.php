@php
    $logged_user = Auth::user();
@endphp

<div id="app-nav" class="hidden-menu">
    <div class="header_container">

        <div class="logo"></div>

        <div class="header">
            <a  href="{{ url('/') }}"
                class="btn_header">
                Inicio
            </a>
            <a  href="{{ url('/#nosotros') }}"
                class="btn_header">
                Nosotros
            </a>

			<a  href="{{ url('/#contacto') }}"
                class="btn_header">
                Contáctanos
            </a>
			
        </div>

       
    </div>
</div>