@extends('layout')

@section('styles')
    @yield('styles')
@endsection

@section('content')
    <div id="inicio">
			<img src="{{ asset('/assets/img/1.jpg')}}" alt="agendas academicas">
    </div>
	<div id="nosotros">
		 
        <div class="d-flex flex-grow-1">
				<div class="col-4 col-md-4">
					<div class="section-header">
					¿Quiénes somos?
					</div>
					<p> La Fundación Amados es una entidad sin ánimo de lucro legalmente constituida e identificada con el NIT: 900.267.022-3; que busca restaurar el tejido humano por medio del Arte. Diseña y ejecuta programas y proyectos claros, responsables y viables para el desarrollo psicosocial, social, afectivo, económico, individual y colectivo de su población objetivo. </p>
				</div>
				<div class="col-4 col-md-4">
					<div class="section-header center">
					Misión
					</div>
					<p>Generar bienestar psicosocial a través del arte a niños, jóvenes y adultos vulnerados, física (maltrato o enfermedad), sexual, social y afectivamente; para que por medio de diferentes manifestaciones artísticas como la danza, el teatro, la música, la cerámica, la literatura y la producción audiovisual; enfocados siempre al entendimiento y comprensión de los valores humanos; logren alcanzar salud mental y sean felices, forjando así un mejor futuro para el país.</p>
				</div>
				<div class="col-4 col-md-4">
					<div class="section-header">
					Visión
					</div>
					<p>La Fundación Amados será una organización líder dentro de la cultura pedagógica del país. Se extenderá a nivel nacional, no solo con el propósito de capacitar a su población objetivo, sino además de incluir a gran parte de la comunidad en el mundo del arte, invirtiendo en capital artístico, social y humano, invitando a estilos de vida saludables con procesos de aceptación para individuos que pertenecen a una sociedad, fomentando participación ciudadana con el fin de generar hechos de paz.</p>
				</div>
		
		</div>
    </div>
	
@endsection

@section('scripts')
    @yield('scripts')
@endsection