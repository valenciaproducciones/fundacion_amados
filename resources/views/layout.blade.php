@extends('layout-base')

@section('styles')
    @yield('styles')
@overwrite

@section('content')
    <div id="amados-app">
        @include('layout-header')
        
        <div id="page-content-root" class="page">
            @yield('content')
        </div>
		<div id="contacto">
			@include('layout-footer')
		</div>
    </div>
@overwrite

@section('scripts')
    @yield('scripts')
@overwrite
